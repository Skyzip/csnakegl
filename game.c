#include <stdio.h>
#include <stdlib.h>

#include "game.h"
#include "glob.h"


bool is_food_on_player(const food_t* food, const snake_t* player)
{
    for (unsigned int i = 0; i < player->length; ++i)
    {
        if (coord_is_same(food->crd, player->body[i]))
            return true;
    }

    return false;
}

food_t food_generate(const snake_t* player)
{
    food_t food = { rand() % (int)PX_WIDTH,
                    rand() % (int)PX_HEIGHT };

    if (player->length < 0.8 * (double)PX_WIDTH * (double)PX_HEIGHT)
    {
        while (is_food_on_player(&food, player))
        {
            food.crd.x = rand() % (int)PX_WIDTH;
            food.crd.y = rand() % (int)PX_HEIGHT;
        }
    }
    else
    {
        unsigned int x, y;
        for (y = 0; y < PX_HEIGHT; ++y)
        {
            for (x = 0; x < PX_HEIGHT; ++x)
            {
                food.crd.x = x;
                food.crd.y = y;

                if (!is_food_on_player(&food, player))
                    return food;
            }
        }
    }

    return food;
}

void snake_grow(snake_t* snake)
{
    coord_t* np = (coord_t*) realloc(snake->body, sizeof(coord_t) * (snake->length + 1));

    if (!np)
    {
        free(snake->body);
        snake->length = 0;

        fprintf(stderr, "Could not allocate enough memory\n");
        exit(EXIT_FAILURE);
    }

    snake->body = np;
    snake->body[snake->length].x = LONG_MIN;
    snake->body[snake->length].y = LONG_MIN;
    ++snake->length;
}

bool snake_try_eat(snake_t* snake, const food_t food)
{
    if (coord_is_same(*snake->body, food.crd))
    {
        snake_grow(snake);
        return true;
    }

    return false;
}

bool snake_try_die(const snake_t* snake)
{
    coord_t head = snake->body[0];
    for (unsigned int i = 4; i < snake->length; ++i)
    {
        if (coord_is_same(head, snake->body[i]))
            return true;
    }

    return false;
}

bool snake_try_wall_hit(snake_t* snake)
{
    coord_t* head = snake->body;

    if (head->x < 0)
    { head->x = PX_WIDTH - 1; return true; }
    else if (head->x >= PX_WIDTH)
    { head->x = 0; return true; }
    else if (head->y < 0)
    { head->y = PX_HEIGHT - 1; return true; }
    else if (head->y >= PX_HEIGHT)
    { head->y = 0; return true; }

    return false;
}
