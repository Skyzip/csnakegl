#ifndef COORD_H
#define COORD_H

#include "boolean.h"


struct Coord
{
    double x;
    double y;
};
typedef struct Coord coord_t;


bool coord_is_same(const coord_t crd1, const coord_t crd2);

#endif // COORD_H
