#ifndef FILE_H
#define FILE_H

#include "config.h"

/// Note: the returned string is dynamically
/// allocated, therefore must be freed when,
/// its lifetime is over
char* file_read(const char* file_name);

unsigned int file_length(const char* file_name);

void file_write_config(const config_t cnf);
bool file_read_config(config_t* cnf);

#endif // FILE_H
