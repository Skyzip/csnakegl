#include <memory.h>
#include <stdlib.h>
#include <stdio.h>
#include <limits.h>

#include "snake.h"


static const int UP_KEY = GLFW_KEY_UP;
static const int DOWN_KEY = GLFW_KEY_DOWN;
static const int LEFT_KEY = GLFW_KEY_LEFT;
static const int RIGHT_KEY = GLFW_KEY_RIGHT;

static direction_t snake_last_direction;
void snake_key_callback(GLFWwindow* window, snake_t* snake)
{
    if (glfwGetKey(window, UP_KEY) == GLFW_PRESS)
    {
        if (snake_last_direction != down)
        { snake->direction = up; }
    }
    else if (glfwGetKey(window, DOWN_KEY) == GLFW_PRESS)
    {
        if (snake_last_direction != up)
        { snake->direction = down; }
    }
    else if (glfwGetKey(window, LEFT_KEY) == GLFW_PRESS)
    {
        if (snake_last_direction != right)
        { snake->direction = left; }
    }
    else if (glfwGetKey(window, RIGHT_KEY) == GLFW_PRESS)
    {
        if (snake_last_direction != left)
        { snake->direction = right; }
    }
}


void lshift(void* array, unsigned int size, unsigned int type_size)
{
    char* dummy = array;
    memcpy(dummy, dummy + type_size, size - type_size);
    //memset(dummy + size - type_size, 0, type_size);
}

void rshift(void* array, unsigned int size, unsigned int type_size)
{
    char* dummy = array;
    memcpy(dummy + type_size, dummy, size - type_size);
    //memset(dummy, 0, type_size);
}


void snake_move(snake_t* snake)
{
    snake_last_direction = snake->direction;
    rshift(snake->body, sizeof(*snake->body) * snake->length, sizeof(*snake->body));

    switch (snake->direction)
    {
    case up:
        --snake->body->y;
        break;
    case down:
        ++snake->body->y;
        break;
    case left:
        --snake->body->x;
        break;
    case right:
        ++snake->body->x;
        break;
    }
}


snake_t snake_allocate(const char* name, unsigned int size, direction_t direction)
{
    snake_t snake;

    snake.body = (coord_t*) malloc(sizeof(coord_t) * size);
    if (!snake.body)
    {
        fprintf(stderr, "Could not allocate enough memory\n");
        exit(EXIT_FAILURE);
    }

    snake.name = name;
    snake.length = size;
    snake.direction = direction;

    return snake;
}

void snake_free(snake_t* snake)
{
    free(snake->body);
}
