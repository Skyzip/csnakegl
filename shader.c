#include <glad/glad.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "shader.h"
#include "file.h"


unsigned int shader_create(const char* vertex, const char* fragment)
{
    unsigned int ID;

    // read shader files
    char* vertexCode = file_read(vertex);
    char* fragmentCode = file_read(fragment);
    if (!(vertexCode || fragmentCode))
        return 0;

    // compile shaders
    unsigned int vShader, fShader;
    // vertex shader
    vShader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vShader, 1, (const char* const*)&vertexCode, NULL);
    glCompileShader(vShader);
    shader_check_compile_errors(vShader, "VERTEX");
    // fragment Shader
    fShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fShader, 1, (const char* const*)&fragmentCode, NULL);
    glCompileShader(fShader);
    shader_check_compile_errors(fShader, "FRAGMENT");

    // shader program
    ID = glCreateProgram();
    glAttachShader(ID, vShader);
    glAttachShader(ID, fShader);
    glLinkProgram(ID);
    shader_check_compile_errors(ID, "PROGRAM");
    // delete the shaders as they're linked into our program now and no longer necessery
    glDeleteShader(vShader);
    glDeleteShader(fShader);

    if (vertexCode)
        free(vertexCode);
    if (fragmentCode)
        free(fragmentCode);

    return ID;
}

void shader_check_compile_errors(const unsigned int shader, const char* type)
{
    GLint success;
    GLchar log[1024];
    if(strcmp(type, "PROGRAM"))
    {
        glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
        if(!success)
        {
            glGetShaderInfoLog(shader, 1024, NULL, log);
            printf("ERROR::SHADER_COMPILATION_ERROR of type: %s\n", type);
            printf("%s\n", log);
        }
    }
    else
    {
        glGetProgramiv(shader, GL_LINK_STATUS, &success);
        if(!success)
        {
            glGetProgramInfoLog(shader, 1024, NULL, log);
            printf("ERROR::PROGRAM_LINKING_ERROR of type: %s\n", type);
            printf("%s\n", log);
        }
    }
}
