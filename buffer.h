#ifndef BUFFER_H
#define BUFFER_H

typedef unsigned int vao;
typedef unsigned int vbo;
typedef unsigned int ebo;
typedef unsigned int ibo;
typedef struct
{
    vao VAO;
    vbo VBO;
    ebo EBO;
    ibo IBO;
} buffer_t;

#endif // BUFFER_H
