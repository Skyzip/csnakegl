#ifndef GAME_H
#define GAME_H

#include "snake.h"
#include "food.h"


void snake_key_callback(GLFWwindow* window, snake_t* snake);
food_t food_generate(const snake_t* player);
bool snake_try_eat(snake_t* snake, const food_t food);
bool snake_try_die(const snake_t* snake);
bool snake_try_wall_hit(snake_t* snake);

#endif // GAME_H
