#ifndef SNAKE_H
#define SNAKE_H

#include <GLFW/glfw3.h>

#include "coord.h"
#include "direction.h"


typedef struct
{
    const char* name;
    coord_t* body;
    unsigned int length;
    direction_t direction;
} snake_t;


void snake_key_callback(GLFWwindow* window, snake_t* snake);
void snake_move(snake_t* snake);

snake_t snake_allocate(const char* name, unsigned int size, direction_t direction);
void snake_free(snake_t* snake);

#endif // SNAKE_H
