#include <stdio.h>
#include <stdlib.h>

#include "file.h"

static const char* CONFIG_FILE = "conf.ini";

char* file_read(const char* fname)
{
    FILE* fp = fopen(fname, "r");

    if (fp)
    {
        unsigned int length = file_length(fname) + 1;

        char* buffer = (char*) malloc(length * sizeof(char));
        char* b = buffer;

        while (fscanf(fp, "%c", b) == 1)
        {
            ++b;
        }
        *b = '\x0';

        fclose(fp);

        return buffer;
    }

    printf("Failed to open file: %s\n", fname);
    return NULL;
}


unsigned int file_length(const char* fname)
{
    FILE* fp = fopen(fname, "r");
    unsigned int len = 0;

    if (fp)
    {
        fseek(fp, 0, SEEK_END);
        len = (unsigned int)ftell(fp);
        fclose(fp);
    }

    return len;
}


void file_write_config(const config_t cnf)
{
    FILE* fp = fopen(CONFIG_FILE, "w");

    if (fp)
    {
        // snake config
        fprintf(fp, "%s %d %d ", cnf.snake.name, cnf.snake.direction, cnf.snake.length);

        fprintf(fp, "%lf %lf", cnf.snake.body[0].x, cnf.snake.body[0].y);
        for (unsigned int i = 1; i < cnf.snake.length; ++i)
        {
            fprintf(fp, " %lf %lf", cnf.snake.body[i].x, cnf.snake.body[i].y);
        }
        fprintf(fp, "\n");

        // food config
        fprintf(fp, "%lf %lf\n", cnf.food.crd.x, cnf.food.crd.y);

        // timeout config
        fprintf(fp, "%lf\n", cnf.timeout);

        // screen config
        fprintf(fp, "%u %u %u\n", cnf.pixel_width, cnf.pixel_height, cnf.pixel_size);

        fclose(fp);
    }
    else
    {
        fprintf(stderr, "Failed to write config file.\n");
    }
}

bool file_read_config()
{
    FILE* fp = fopen(CONFIG_FILE, "r");

    if (fp)
    {

    }

    return false;
}

