#ifndef CONFIG_H
#define CONFIG_H

#include "snake.h"
#include "food.h"

typedef struct
{
    snake_t snake;
    food_t food;
    unsigned int pixel_width;
    unsigned int pixel_height;
    unsigned int pixel_size;
    double timeout;
} config_t;

#endif // CONFIG_H
