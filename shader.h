#ifndef SHADER_H
#define SHADER_H

unsigned int shader_create(const char* vertex, const char* fragment);
void shader_check_compile_errors(const unsigned int shader, const char* type);

#endif // SHADER_H
