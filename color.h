#ifndef COLOR_H
#define COLOR_H

typedef struct
{
    double r;
    double g;
    double b;
} color_t;

#endif // COLOR_H
