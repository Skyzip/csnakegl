#include <glad/glad.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include "render.h"
#include "glob.h"
#include "color.h"
#include "buffer.h"


static coord_t* snake_translations = NULL;
static coord_t food_offset;
static unsigned int mem = 0;
static buffer_t snake_buffer_objects;
static buffer_t food_buffer_objects;

static color_t snake_color = {.2, .7, .4};
static color_t food_color = {.8, .1, .2};


void alloc_mem()
{
    coord_t* np = (coord_t*) realloc(snake_translations, 2 * mem * sizeof(coord_t));

    if (!np)
    {
        free(snake_translations);
        mem = 0;

        fprintf(stderr, "Could not allocate enough memory\n");
        exit(EXIT_FAILURE);
    }

    snake_translations = np;
    mem *= 2;
}


void gen_snake_buffers()
{
    double vertices[] =
    {
        -1.,                            1. - 2. / (double)PX_HEIGHT,
        snake_color.r, snake_color.g, snake_color.b,    // bottom left

        -1. + 2. / (double)PX_WIDTH,    1. - 2. / (double)PX_HEIGHT,
        snake_color.r, snake_color.g + 0.2, snake_color.b - 0.2,    // bottom right

        -1. + 2. / (double)PX_WIDTH,    1.,
        snake_color.r, snake_color.g, snake_color.b,    // top right

        -1.,                            1.,
        snake_color.r, snake_color.g - 0.2, snake_color.b + 0.2,    // top left
    };
    unsigned int indices[] =
    {
        0, 1, 2,  // first tri
        2, 3, 0   // second tri
    };

    // generate vertex attributes
    glGenVertexArrays(1, &snake_buffer_objects.VAO);
    glGenBuffers(1, &snake_buffer_objects.VBO);
    glGenBuffers(1, &snake_buffer_objects.EBO);
    glGenBuffers(1, &snake_buffer_objects.IBO);

    // bind the vertex attribute object
    glBindVertexArray(snake_buffer_objects.VAO);
    // bind the buffer objects, too (different types of buffers can be bound at the same time)
    glBindBuffer(GL_ARRAY_BUFFER, snake_buffer_objects.VBO);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, snake_buffer_objects.EBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_DYNAMIC_DRAW);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_DYNAMIC_DRAW);

    glEnableVertexAttribArray(0);
    // current object in attribute pointer consists of 2 values
    // the whole attribute's size is 5
    // and the stride is 0 (the first element is the first - no stride)
    glVertexAttribPointer(0, 2, GL_DOUBLE, GL_FALSE, 5 * sizeof(double), (void*)0);

    glEnableVertexAttribArray(1);
    // current object in attribute pointer consists of 3 values
    // the whole attribute's size is 5
    // and the stride is 3 (the first element is the third)
    glVertexAttribPointer(1, 3, GL_DOUBLE, GL_FALSE, 5 * sizeof(double), (void*)(2 * sizeof(double)));


    glEnableVertexAttribArray(2);
    glBindBuffer(GL_ARRAY_BUFFER, snake_buffer_objects.IBO);
    glVertexAttribPointer(2, 2, GL_DOUBLE, GL_FALSE, sizeof(coord_t), (void*)0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    // tell OpenGL this is an instanced vertex attribute.
    glVertexAttribDivisor(2, 1);


    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
}

void gen_food_buffers()
{
    double vertices[] =
    {
        -1.,                            1. - 2. / (double)PX_HEIGHT,
        food_color.r + 0.1, food_color.g, food_color.b,    // bottom left

        -1. + 2. / (double)PX_WIDTH,    1. - 2. / (double)PX_HEIGHT,
        food_color.r - 0.1, food_color.g, food_color.b,    // bottom right

        -1. + 2. / (double)PX_WIDTH,    1.,
        food_color.r + 0.5, food_color.g, food_color.b,    // top right

        -1.,                            1.,
        food_color.r - 0.5, food_color.g, food_color.b,    // top left
    };
    unsigned int indices[] =
    {
        0, 1, 2,  // first tri
        2, 3, 0   // second tri
    };

    glGenVertexArrays(1, &food_buffer_objects.VAO);
    glGenBuffers(1, &food_buffer_objects.VBO);
    glGenBuffers(1, &food_buffer_objects.EBO);
    glGenBuffers(1, &food_buffer_objects.IBO);

    glBindVertexArray(food_buffer_objects.VAO);
    glBindBuffer(GL_ARRAY_BUFFER, food_buffer_objects.VBO);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, food_buffer_objects.EBO);

    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_DYNAMIC_DRAW);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_DYNAMIC_DRAW);

    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glEnableVertexAttribArray(2);

    glVertexAttribPointer(0, 2, GL_DOUBLE, GL_FALSE, 5 * sizeof(double), (void*)0);
    glVertexAttribPointer(1, 3, GL_DOUBLE, GL_FALSE, 5 * sizeof(double), (void*)(2 * sizeof(double)));

    glBindBuffer(GL_ARRAY_BUFFER, food_buffer_objects.IBO);
    glVertexAttribPointer(2, 2, GL_DOUBLE, GL_FALSE, sizeof(coord_t), (void*)0);
    glVertexAttribDivisor(2, 1);


    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
}

void update_snake_instances()
{
    glBindBuffer(GL_ARRAY_BUFFER, snake_buffer_objects.IBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(*snake_translations) * mem, snake_translations, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void update_food_instances()
{
    glBindBuffer(GL_ARRAY_BUFFER, food_buffer_objects.IBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(coord_t), &food_offset, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void render_init(const unsigned int init_mem)
{
    snake_translations = (coord_t*) malloc(init_mem * sizeof(coord_t));
    if (!snake_translations)
    {
        fprintf(stderr, "Could not allocate enough memory\n");
        exit(EXIT_FAILURE);
    }

    mem = init_mem;

    gen_snake_buffers();
    gen_food_buffers();
}

void render_end(void)
{
    if (snake_translations)
        free(snake_translations);
    mem = 0;
}

coord_t pton(const coord_t pix_coord)
{
    coord_t norm_coord = { 2. * pix_coord.x / (double)PX_WIDTH,
                        -2. * pix_coord.y / (double)PX_HEIGHT };
    return norm_coord;
}

void make_trans(const snake_t snake)
{
    for (unsigned int i = 0; i < snake.length; ++i)
    {
        snake_translations[i] = pton(snake.body[i]);
    }
}

void render_snake(const snake_t snake)
{
    if (snake.length > mem)
    {
        alloc_mem();
    }

    // calculate "on-screen" translations
    make_trans(snake);
    update_snake_instances();

    // uncomment this call to draw in wireframe polygons.
    //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    // bind all the data stored in VAO
    glBindVertexArray(snake_buffer_objects.VAO);
    // we do not bind an instance array (hence (void*)0)
    // we are already using an element buffer object bound to the VAO
    glDrawElementsInstanced(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (void*)0, (GLsizei)snake.length);
    glBindVertexArray(0);
}

void render_food(const food_t food)
{
    food_offset = pton(food.crd);
    update_food_instances();

    glBindVertexArray(food_buffer_objects.VAO);
    glDrawElementsInstanced(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (void*)0, 1);
    glBindVertexArray(0);
}
