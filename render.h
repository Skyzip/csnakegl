#ifndef RENDER_H
#define RENDER_H

#include "snake.h"
#include "food.h"

void render_init(const unsigned int mem);
void render_end(void);

void render_snake(const snake_t snake);
void render_food(const food_t food);

#endif // RENDER_H
