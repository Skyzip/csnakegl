cmake_minimum_required (VERSION 3.0)

project (snake2 LANGUAGES C)
set (CMAKE_C_STANDARD 11)


set (GLFW_DIR "${CMAKE_CURRENT_SOURCE_DIR}/glfw-3.3.1")

set (CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/lib)
set (CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/lib)
set (CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/bin)

set (GLFW_BUILD_DOCS OFF CACHE BOOL "" FORCE)
set (GLFW_BUILD_TESTS OFF CACHE BOOL "" FORCE)
set (GLFW_BUILD_EXAMPLES OFF CACHE BOOL "" FORCE)
set (GLFW_INSTALL OFF CACHE BOOL "" FORCE)


add_subdirectory (${GLFW_DIR})

message ("Generating snake2 ...")

include_directories ("${GLFW_DIR}/include" "${CMAKE_CURRENT_SOURCE_DIR}/glad/include")
link_directories ("${CMAKE_CURRENT_SOURCE_DIR}/lib")

add_executable (snake2 glad/src/glad.c main.c shader.c file.c render.c snake.c food.c coord.c game.c)
target_link_libraries (snake2 glfw)
