#ifndef GLOB_H
#define GLOB_H

typedef unsigned int pix;

extern pix PX_WIDTH;
extern pix PX_HEIGHT;
extern pix PX_SIZE;

#endif // GLOB_H
