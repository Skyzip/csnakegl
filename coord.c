#include "coord.h"

bool coord_is_same(const coord_t crd1, const coord_t crd2)
{
    return (crd1.x == crd2.x && crd1.y == crd2.y);
}
