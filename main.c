#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include "coord.h"
#include "file.h"
#include "shader.h"
#include "render.h"
#include "glob.h"
#include "snake.h"
#include "food.h"
#include "boolean.h"
#include "game.h"


// extern vars from glob.h
pix PX_WIDTH = 100;
pix PX_HEIGHT = 50;
pix PX_SIZE = 10;

static unsigned int SCR_WIDTH;
static unsigned int SCR_HEIGHT;

static double timeout = 200;
static double time_diff = 10;
static bool pause = false;
static config_t config;

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    switch (action) {
    case GLFW_PRESS:
        switch (key)
        {
        case GLFW_KEY_ESCAPE:
            glfwSetWindowShouldClose(window, 1);
            break;
        case GLFW_KEY_KP_ADD:
            timeout -= time_diff;
            if (timeout < 30) timeout = 30;
            break;
        case GLFW_KEY_KP_SUBTRACT:
            timeout += time_diff;
            if (timeout > 500) timeout = 500;
            break;
        case GLFW_KEY_P:
            pause = true;
            break;
        case GLFW_KEY_0:
            //file_write_config(config);
            break;

        default:
            pause = false;
            break;
        }

        break;
    }
}

void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
    glViewport(0, 0, width, height);
    glfwGetWindowSize(window, (int*)&SCR_WIDTH, (int*)&SCR_HEIGHT);
}

int game()
{
    // glfw window creation
    PX_WIDTH = 100;
    PX_HEIGHT = 50;
    PX_SIZE = 10;
    SCR_WIDTH = PX_SIZE * PX_WIDTH;
    SCR_HEIGHT = PX_SIZE * PX_HEIGHT;
    GLFWwindow* window = glfwCreateWindow((int)SCR_WIDTH, (int)SCR_HEIGHT, "Snake2", NULL, NULL);
    if (window == NULL)
    {
        printf("Failed to create GLFW window.\n");
        glfwTerminate();
        return -1;
    }
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
    glfwSetKeyCallback(window, key_callback);
    glfwMakeContextCurrent(window);

    // glad: load all OpenGL function pointers
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        printf("Failed to initialize GLAD.\n");
        return -1;
    }

    // program setup
    unsigned int shaderID = shader_create("../basic.vs", "../basic.fs");
    if (!shaderID)
    {
        printf("Failed to create shaders.\n");
        return -1;
    }


    snake_t player = snake_allocate("skyzip", 3, right);
    player.body[0].x = 2; player.body[0].y = 10;
    player.body[1].x = 1; player.body[1].y = 10;
    player.body[2].x = 0; player.body[2].y = 10;
    bool eaten, die, wall_hit;

    food_t food = food_generate(&player);

    // render loop
    render_init(100);
    glfwSetTime(0);
    double past = glfwGetTime(), delta;
    while (!glfwWindowShouldClose(window))
    {
        if (!pause)
        {
            delta = glfwGetTime() - past;

            if (1000 * delta > timeout)
            {
                past = glfwGetTime();

                snake_move(&player);
                eaten = snake_try_eat(&player, food);
                die = snake_try_die(&player);
                wall_hit = snake_try_wall_hit(&player);

                if (die || wall_hit)
                {
                    printf("%s\n", die ? "You died ..." : "You hit the wall ...");
                    break;
                }

                if (eaten)
                { food = food_generate(&player); }
            }

            snake_key_callback(window, &player);

            // render
            glClearColor(0.3f, 0.1f, 0.7f, 1.0f);
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

            glUseProgram(shaderID);
            render_snake(player);
            render_food(food);
        }

        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    // kill it!!!
    render_end();
    snake_free(&player);
    glfwTerminate();

    return 0;
}

int main()
{
    srand((unsigned int)time(NULL));
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    return game();
}
