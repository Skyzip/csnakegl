#ifndef DIRECTION_H
#define DIRECTION_H

typedef enum
{
    up,
    down,
    left,
    right
} direction_t;

#endif // DIRECTION_H
